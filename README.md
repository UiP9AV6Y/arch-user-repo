# Arch Linux User Repository

Automated [package building][] for Arch Linux. Software is built using Gitlab-CI
and uploaded to the codeberg.org [package repository][].

[package building]: https://wiki.archlinux.org/title/Arch_User_Repository
[package repository]: https://codeberg.org/UiP9AV6Y/arch-user-repo/packages

## Setup

Retrieve the signing key to enable the installation of packages:

```bash
curl -sLO https://codeberg.org/api/packages/UiP9AV6Y/arch/repository.key
sudo pacman-key --add repository.key
sudo pacman-key --lsign-key 'UiP9AV6Y@noreply.codeberg.org'
```

Configure pacman to use the repository for packages by appending
the following settings to */etc/pacman.conf*:

```bash
cat <<INI | sudo tee -a /etc/pacman.conf
[UiP9AV6Y]
SigLevel = Required
Server = https://codeberg.org/api/packages/UiP9AV6Y/arch/UiP9AV6Y-AUR/$arch
INI
```

If you have multiple repositories configured, you can include the repository
name in installation instructions, should a packages exists in more than one
place:

`pacman -S UiP9AV6Y/gomplate`

