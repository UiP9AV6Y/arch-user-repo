PACKAGES = $(notdir $(patsubst %/,%,$(dir $(wildcard */Makefile))))

.PHONY: default
default: help

.PHONY: help
help:
	# Commands:
	# make all - set up the build environment and build all packages
	# make list - show available packages
	#
	# Development commands:
	# make build - build all available packages
	#
	# Testing commands:
	# make test - run package tests
	#
	# Deployment commands:
	# make publish - upload built packages

.PHONY: all
all: setup build

.PHONY: list
list:
	@for PKG in $(PACKAGES); do \
	  echo '#' $$PKG ; \
	done

.PHONY: $(PACKAGES)
$(PACKAGES):
	$(MAKE) build-$@

.PHONY: build
build: $(addprefix build-,$(PACKAGES))

.PHONY: build-%
build-%:
	$(MAKE) -C $* pkg \
	  PKG_DIR=$(abspath build-$*)

.PHONY: publish
publish: $(addprefix publish-,$(PACKAGES))

.PHONY: publish-%
publish-%:
	$(MAKE) -C $* publish \
	  PKG_DIR=$(abspath build-$*)

.PHONY: test
test: $(addprefix test-,$(PACKAGES))

.PHONY: test-%
test-%:
	$(MAKE) -C $* test \
	  PKG_DIR=$(abspath build-$*)

.PHONY: clean-local
clean-local:
	-$(RM) -r build-*

.PHONY: clean
clean: clean-local
	@for PKG in $(PACKAGES); do \
	  $(MAKE) -C $$PKG $@ || \
	  exit 1 ; \
	done

.PHONY: mr-proper
mr-proper: clean
	@for PKG in $(PACKAGES); do \
	  $(MAKE) -C $$PKG $@ || \
	  exit 1 ; \
	done

