SOURCE_DIR ?= src
BUILD_DIR ?= pkg
PKG_DIR ?= $(BUILD_DIR)
PKG_NAME ?= $(notdir $(CURDIR))
PKG_SOURCE ?= https://aur.archlinux.org/$(PKG_NAME).git
PKG_VENDOR ?= Unknown Packager

FORGE_HOST ?= code.forgejo.org
FORGE_OWNER ?= $(BUILD_USER)
FORGE_GROUP ?= $(FORGE_OWNER)-AUR
FORGE_USERNAME ?= $(FORGE_OWNER)
FORGE_PASSWORD ?= $(FORGE_USERNAME)

GIT ?= git
ENV ?= env
EDIT ?= sed
CURL ?= curl
PACMAN ?= pacman
INSTALL ?= install
INSTALL_DIR ?= $(INSTALL) -d
INSTALL_DATA ?= $(INSTALL) -m 644
INSTALL_PROGRAM ?= $(INSTALL)
MAKEPKG ?= makepkg

MAKEPKG_REAL = $(shell which $(MAKEPKG))
MAKEPKG_BIN := $(abspath $(BUILD_DIR)/BUILDROOT/bin/makepkg)
MAKEPKG_BUILD_DIR := $(abspath $(BUILD_DIR)/BUILD)
MAKEPKG_PKG_DIR := $(abspath $(PKG_DIR))
MAKEPKG_SRC_DIR := $(abspath $(BUILD_DIR)/SOURCES)
MAKEPKG_LOG_DIR := $(abspath $(BUILD_DIR)/LOGS)
MAKEPKG_ENV := \
  "PACKAGER=$(PKG_VENDOR)" \
  "BUILDDIR=$(MAKEPKG_BUILD_DIR)" \
  "SRCPKGDEST=$(MAKEPKG_PKG_DIR)" \
  "PKGDEST=$(MAKEPKG_PKG_DIR)" \
  "SRCDEST=$(MAKEPKG_SRC_DIR)" \
  "LOGDEST=$(MAKEPKG_LOG_DIR)"

ARTIFACT ?= $(PKG_DIR)/$(PKG_NAME).pkg.tar.zst

export SOURCE_DATE_EPOCH := 0

.PHONY: default
default: help

.PHONY: help
help:
	# Commands:
	# make all - set up the build environment and build all packages
	# make info - show packaging strategy
	#
	# Development commands:
	# make build - build all available packages
	#
	# Testing commands:
	# make test - run package tests
	#
	# Deployment commands:
	# make provision - install built packages
	# make publish - upload built packages

.PHONY: info
info:
ifneq ($(wildcard PKGBUILD),)
	@echo "Package $(PKG_NAME) is build from local manifest"
else
	@echo "Package $(PKG_NAME) is build from remote manifest: $(PKG_SOURCE)"
endif

.PHONY: all
all: validate build

.PHONY: build pkg
build pkg: .PKGS

.PHONY: update-srcinfo
update-srcinfo: .SRCINFO

.PHONY: publish
publish:
	@for PKG in $(wildcard $(PKG_DIR)/*.pkg.tar.*); do \
	  $(MAKE) upload ARTIFACT=$$PKG || \
	  exit 1 ; \
	done

.PHONY: upload
upload: $(ARTIFACT)
	@echo "Uploading $< to $(FORGE_HOST) ($(FORGE_OWNER)/$(FORGE_GROUP))"
	@$(CURL) --no-progress-meter -X PUT \
	  https://$(FORGE_HOST)/api/packages/$(FORGE_OWNER)/arch/$(FORGE_GROUP) \
	  --user $(FORGE_USERNAME):$(FORGE_PASSWORD) \
	  --header 'Content-Type: application/octet-stream' \
	  --data-binary @$<

.PHONY: test
test:
	@for PKG in $(wildcard $(PKG_DIR)/*.pkg.tar.*); do \
	  $(MAKE) verify ARTIFACT=$$PKG || \
	  exit 1 ; \
	done

.PHONY: verify
verify: $(ARTIFACT)
	@echo "Verifying $<"
	@$(PACMAN) -Qip $<
ifneq ($(wildcard test-runner),)
	@echo "Running acceptance test suite against $<"
	./test-runner $<
endif

.PHONY: provision
provision:
	@for PKG in $(wildcard $(PKG_DIR)/*.pkg.tar.*); do \
	  $(MAKE) install ARTIFACT=$$PKG || \
	  exit 1 ; \
	done

.PHONY: install
install: $(ARTIFACT)
	@echo "Installing $<"
	@$(PACMAN) -U --noconfirm $<

.PHONY: validate
validate: shellcheck namcap

.PHONY: shellcheck
ifneq ($(wildcard PKGBUILD),)
shellcheck: PKGBUILD
else
shellcheck: $(SOURCE_DIR)/PKGBUILD
endif
	shellcheck --shell=bash $<

.PHONY: namcap
ifneq ($(wildcard PKGBUILD),)
namcap: PKGBUILD
else
namcap: $(SOURCE_DIR)/PKGBUILD
endif
	namcap --info $<

$(SOURCE_DIR)/PKGBUILD:
	$(GIT) clone --depth=1 \
		$(PKG_SOURCE) $(@D)

ifneq ($(wildcard PKGBUILD),)
.SRCINFO: PKGBUILD $(MAKEPKG_BIN)
	$(ENV) $(MAKEPKG_ENV) $(MAKEPKG_BIN) --printsrcinfo \
	> $@
else
.SRCINFO: $(SOURCE_DIR)/PKGBUILD $(MAKEPKG_BIN)
	$(ENV) -C $(SOURCE_DIR) $(MAKEPKG_ENV) $(MAKEPKG_BIN) --printsrcinfo \
	> $@
endif

ifneq ($(wildcard PKGBUILD),)
.PKGS: PKGBUILD $(MAKEPKG_BIN)
	$(ENV) $(MAKEPKG_ENV) $(MAKEPKG_BIN) --syncdeps --force --noconfirm 
else
.PKGS: $(SOURCE_DIR)/PKGBUILD $(MAKEPKG_BIN)
	$(ENV) -C $(SOURCE_DIR) $(MAKEPKG_ENV) $(MAKEPKG_BIN) --syncdeps --force --noconfirm
endif
	ls -1 $(PKG_DIR) \
	| grep -F pkg.tar \
	| grep -Fv .sig \
	> $@

$(MAKEPKG_BIN): $(MAKEPKG_REAL)
	$(EDIT) \
	  -e '/exit $$E_ROOT/d' \
	  $< \
	| $(INSTALL_PROGRAM) -D -m 755 /dev/stdin $@

.PHONY: clean-local
clean-local:

.PHONY: clean
clean: clean-local
	$(RM) $(PKG_DIR)/*.tar.* .PKGS

.PHONY: mr-proper
mr-proper: clean
	$(RM) -r $(SOURCE_DIR) $(BUILD_DIR)

